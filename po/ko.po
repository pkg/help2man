# Korean translation of help2man.
# Copyright (C) 2022 Free Software Foundation, Inc.
# This file is distributed under the same license as the help2man package.
# Seong-ho Cho <darkcircle.0426@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: help2man 1.47.17\n"
"Report-Msgid-Bugs-To: Brendan O'Dea <bug-help2man@gnu.org>\n"
"POT-Creation-Date: 2022-12-13 11:16+1100\n"
"PO-Revision-Date: 2022-12-09 02:48+0900\n"
"Last-Translator: Seong-ho Cho <darkcircle.0426@gmail.com>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"X-Generator: Poedit 2.3.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: help2man:108
#, fuzzy, perl-format
msgid ""
"GNU %s %s\n"
"\n"
"Copyright (C) 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2009,\n"
"2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2020, 2021, 2022 Free "
"Software\n"
"Foundation, Inc.\n"
"This is free software; see the source for copying conditions.  There is NO\n"
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
"\n"
"Written by Brendan O'Dea <bod@debian.org>\n"
msgstr ""
"GNU %s %s\n"
"\n"
"Copyright (C) 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2009,\n"
"2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2020, 2021 Free Software\n"
"Foundation, Inc.\n"
"이 프로그램은 자유 소프트웨어입니다. COPYING 조항은 소스코드를 살펴보십시"
"오.  상업성 또는\n"
"특정 목적의 적합성에 조차도 보증하지 않습니다.\n"
"\n"
"Brendan O'Dea <bod@debian.org>가 작성함\n"

#: help2man:120
#, perl-format
msgid ""
"`%s' generates a man page out of `--help' and `--version' output.\n"
"\n"
"Usage: %s [OPTION]... EXECUTABLE\n"
"\n"
" -n, --name=STRING       description for the NAME paragraph\n"
" -s, --section=SECTION   section number for manual page (1, 6, 8)\n"
" -m, --manual=TEXT       name of manual (User Commands, ...)\n"
" -S, --source=TEXT       source of program (FSF, Debian, ...)\n"
" -L, --locale=STRING     select locale (default \"C\")\n"
" -i, --include=FILE      include material from `FILE'\n"
" -I, --opt-include=FILE  include material from `FILE' if it exists\n"
" -o, --output=FILE       send output to `FILE'\n"
" -p, --info-page=TEXT    name of Texinfo manual\n"
" -N, --no-info           suppress pointer to Texinfo manual\n"
" -l, --libtool           exclude the `lt-' from the program name\n"
"     --help              print this help, then exit\n"
"     --version           print version number, then exit\n"
"\n"
"EXECUTABLE should accept `--help' and `--version' options and produce output "
"on\n"
"stdout although alternatives may be specified using:\n"
"\n"
" -h, --help-option=STRING     help option string\n"
" -v, --version-option=STRING  version option string\n"
" --version-string=STRING      version string\n"
" --no-discard-stderr          include stderr when parsing option output\n"
"\n"
"Report bugs to <bug-help2man@gnu.org>.\n"
msgstr ""
"`%s'은(는) `--help'의 설명서 페이지 출력과 `--version' 출력을 만듭니다.\n"
"\n"
"사용법: %s [<옵션>]... <실행파일>\n"
"\n"
" -n, --name=<문자열>         이름 문단 설명\n"
" -s, --section=<섹션>        설명서 페이지 섹션 번호\n"
" -m, --manual=<텍스트>       설명서 이름 (사용자 명령, ...)\n"
" -S, --source=<텍스트>       프로그램 공급원 (FSF, 데비안, ...)\n"
" -L, --locale=<문자열>       로캘 선택 (기본: \"C\")\n"
" -i, --include=<파일>        `<파일>' 내용 포함\n"
" -I, --opt-include=<파일>    `<파일>'이 있다면 내용 포함\n"
" -o, --output=<파일>         `<파일>'로 출력 보내기\n"
" -p, --info-page=<텍스트>    Texinfo 설명서 이름\n"
" -N, --no-info               Texinfo 설명서 포인터 없애기\n"
" -l, --libtool               프로그램 이름에서 `lt-' 제외\n"
"     --help                  이 도움말을 출력하고 나갑니다\n"
"     --version               버전 번호를 출력하고 나갑니다\n"
"\n"
"<실행파일> 은 `--help' 옵션과 `--version' 옵션을 받아들여야 하며\n"
"다음 웁션을 대신 지정한다 하더라도 표준 출력으로 결과를 내보내야합니다:\n"
"\n"
" -h, --help-option=<문자열>     도움말 옵션 문자열\n"
" -v, --version-option=<문자열>  버전 옵션 문자열\n"
" --version-string=<문자열>      버전 문자열\n"
" --no-discard-stderr            옵션 출력을 해석할 때 표준 출력도 포함\n"
"\n"
"<bug-help2man@gnu.org>에 버그를 알려주십시오.\n"

#: help2man:218
#, perl-format
msgid "%s: can't open `%s' (%s)"
msgstr "%s: `%s'을(를) 열 수 없음(%s)"

#. Translators: "NAME", "SYNOPSIS" and other one or two word strings in all
#. upper case are manual page section headings.  The man(1) manual page in your
#. language, if available should provide the conventional translations.
#: help2man:244 help2man:377 help2man:383 help2man:734 help2man.h2m.PL:88
#: help2man.h2m.PL:138
msgid "NAME"
msgstr "이름"

#: help2man:244 help2man:452 help2man:734 help2man.h2m.PL:139
msgid "SYNOPSIS"
msgstr "요약"

#: help2man:294
#, perl-format
msgid "%s: no valid information found in `%s'"
msgstr "%s: `%s'에 적절한 정보가 없습니다"

#. Translators: the following message is a strftime(3) format string, which in
#. the English version expands to the month as a word and the full year.  It
#. is used on the footer of the generated manual pages.  If in doubt, you may
#. just use %x as the value (which should be the full locale-specific date).
#: help2man:324
msgid "%B %Y"
msgstr "%Y년 %B"

#: help2man:331
#, perl-format
msgid "%s: can't unlink %s (%s)"
msgstr "%s: %s 링크를 해제할 수 없음(%s)"

#: help2man:335
#, perl-format
msgid "%s: can't create %s (%s)"
msgstr "%s: %s을(를) 만들 수 없음(%s)"

#: help2man:391
#, perl-format
msgid "%s \\- manual page for %s %s"
msgstr "%s \\- %s %s 설명서 페이지"

#: help2man:405
msgid "System Administration Utilities"
msgstr "시스템 관리 유틸리티"

#: help2man:406
msgid "Games"
msgstr "게임"

#: help2man:407
msgid "User Commands"
msgstr "사용자 명령"

#. Translators: "Usage" and "or" here are patterns (regular expressions) which
#. are used to match the usage synopsis in program output.  An example from cp
#. (GNU coreutils) which contains both strings:
#. Usage: cp [OPTION]... [-T] SOURCE DEST
#. or:  cp [OPTION]... SOURCE... DIRECTORY
#. or:  cp [OPTION]... -t DIRECTORY SOURCE...
#: help2man:418
msgid "Usage"
msgstr "사용법"

#: help2man:419
msgid "or"
msgstr "또는"

#: help2man:456 help2man:734 help2man.h2m.PL:140
msgid "DESCRIPTION"
msgstr "설명"

#. Translators: patterns are used to match common program output. In the source
#. these strings are all of the form of "my $PAT_something = _('...');" and are
#. regular expressions.  If there is more than one commonly used string, you
#. may separate alternatives with "|".  Spaces in these expressions are written
#. as " +" to indicate that more than one space may be matched.  The string
#. "(?:[\\w-]+ +)?" in the bug reporting pattern is used to indicate an
#. optional word, so that either "Report bugs" or "Report _program_ bugs" will
#. be matched.
#: help2man:481
msgid "Report +(?:[\\w-]+ +)?bugs|Email +bug +reports +to"
msgstr "Report +(?:[\\w-]+ +)?bugs|Email +bug +reports +to"

#: help2man:482
msgid "Written +by"
msgstr "Written +by"

#: help2man:483
msgid "Options"
msgstr "옵션"

#: help2man:484
msgid "Environment"
msgstr "환경"

#: help2man:485
msgid "Files"
msgstr "파일"

#: help2man:486
msgid "Examples"
msgstr "예제"

#: help2man:487
msgid "This +is +free +software"
msgstr "This +is +free +software"

#: help2man:501 help2man:734 help2man.h2m.PL:141
msgid "OPTIONS"
msgstr "옵션"

#: help2man:506 help2man:735 help2man.h2m.PL:143
msgid "ENVIRONMENT"
msgstr "환경"

#: help2man:511 help2man:735 help2man.h2m.PL:144
msgid "FILES"
msgstr "파일"

#: help2man:516 help2man:639 help2man:735 help2man.h2m.PL:145
msgid "EXAMPLES"
msgstr "폐제"

#: help2man:532 help2man:660 help2man:736 help2man.h2m.PL:148
msgid "COPYRIGHT"
msgstr "저작권"

#: help2man:538 help2man:666 help2man:736 help2man.h2m.PL:147
msgid "REPORTING BUGS"
msgstr "버그 보고"

#: help2man:544 help2man:735 help2man.h2m.PL:146
msgid "AUTHOR"
msgstr "저자"

#: help2man:698 help2man:736 help2man.h2m.PL:149
msgid "SEE ALSO"
msgstr "추가 참조"

#: help2man:701
#, perl-format
msgid ""
"The full documentation for\n"
".B %s\n"
"is maintained as a Texinfo manual.  If the\n"
".B info\n"
"and\n"
".B %s\n"
"programs are properly installed at your site, the command\n"
".IP\n"
".B info %s\n"
".PP\n"
"should give you access to the complete manual.\n"
msgstr ""
".B %s\n"
"의 전체 문서는 Texinfo 설명서로 관리합니다. \n"
".B info\n"
"와\n"
".B %s\n"
"프로그램을 제대로 설치했다면,\n"
".IP\n"
".B info %s\n"
".PP\n"
"명령으로 완전한 설명서를 띄울 수 있어야합니다.\n"

#: help2man:767
#, perl-format
msgid "%s: error writing to %s (%s)"
msgstr "%s: %s 기록 오류 (%s)"

#: help2man:793
#, perl-format
msgid "%s: can't get `%s' info from %s%s"
msgstr "%s: `%s' 정보를 %s%s(에)서 가져올 수 없음"

#: help2man:795
msgid "Try `--no-discard-stderr' if option outputs to stderr"
msgstr ""
"옵션 출력을 표준 출력으로 내보내려면 `--no-discard-stderr' 옵션을 사용하십시"
"오"

#: help2man.h2m.PL:83
msgid "Include file for help2man man page"
msgstr "help2man 설명서 페이지에 파일 넣기"

#: help2man.h2m.PL:89
msgid "help2man \\- generate a simple manual page"
msgstr "help2man \\- 간단한 설명서 페이지를 만듭니다"

#: help2man.h2m.PL:92
msgid "INCLUDE FILES"
msgstr "파일 포함"

#: help2man.h2m.PL:94
msgid ""
"Additional material may be included in the generated output with the\n"
".B \\-\\-include\n"
"and\n"
".B \\-\\-opt\\-include\n"
"options.  The format is simple:\n"
"\n"
"    [section]\n"
"    text\n"
"\n"
"    /pattern/\n"
"    text\n"
msgstr ""
"추가 내용은\n"
".B \\-\\-include\n"
"과\n"
".B \\-\\-opt\\-include\n"
"옵션으로 만든 출력 내용에 들어갑니다. 형식은 간단합니다:\n"
"\n"
"    [섹션]\n"
"    내용\n"
"\n"
"    /패턴/\n"
"    내용\n"

#: help2man.h2m.PL:109
msgid ""
"Blocks of verbatim *roff text are inserted into the output either at\n"
"the start of the given\n"
".BI [ section ]\n"
"(case insensitive), or after a paragraph matching\n"
".BI / pattern /\\fR.\n"
msgstr ""
"*roff 텍스트 블록은 주어진\n"
".BI [ 섹션 ]\n"
"(대소문자 무관)으로 시작하거나, 또는\n"
".BI / 패턴 /\\fR\n"
"과 일치하는 문단 다음에 있는 그대로 들어갑니다.\n"

#: help2man.h2m.PL:118
msgid ""
"Patterns use the Perl regular expression syntax and may be followed by\n"
"the\n"
".IR i ,\n"
".I s\n"
"or\n"
".I m\n"
"modifiers (see\n"
".BR perlre (1)).\n"
msgstr ""
"패턴은 펄 정규 표현식 문법을 활용하며, \n"
".IR i ,\n"
".I s\n"
"또는\n"
".I m\n"
"수정자 (\n"
".BR perlre(1) 참고)를 따릅니다.\n"

#: help2man.h2m.PL:130
msgid ""
"Lines before the first section or pattern which begin with `\\-' are\n"
"processed as options.  Anything else is silently ignored and may be\n"
"used for comments, RCS keywords and the like.\n"
msgstr ""
"처음 섹션이전 행이나 '\\-'으로 시작하는 패턴은 옵션으로 처리합니다.\n"
"그 박의 내용은 조용히 무시하며 주석, RCS 키워드 등의 용도로 사용합니다.\n"

#: help2man.h2m.PL:136
msgid "The section output order (for those included) is:"
msgstr "섹션 출력 (포함) 순서:"

#: help2man.h2m.PL:142
msgid "other"
msgstr "기타"

#: help2man.h2m.PL:153
msgid ""
"Any\n"
".B [NAME]\n"
"or\n"
".B [SYNOPSIS]\n"
"sections appearing in the include file will replace what would have\n"
"automatically been produced (although you can still override the\n"
"former with\n"
".B \\-\\-name\n"
"if required).\n"
msgstr ""
"파일에 들어간 임의의\n"
".B [이름]\n"
"또는\n"
".B [요약]\n"
"섹션은 자동으로 만든 내용으로 (비록 필요한 경우\n"
".B \\-\\-name\n"
"으로 옵션 우선 적용을 할 수 있지만) 바뀝니다.\n"

#: help2man.h2m.PL:166
msgid ""
"Other sections are prepended to the automatically produced output for\n"
"the standard sections given above, or included at\n"
".I other\n"
"(above) in the order they were encountered in the include file.\n"
msgstr ""
"다른 섹션은 위에 제시한 표준 섹션에 자동으로 만든 출력 다음에 붙거나\n"
".I 기타\n"
"(위에) 넣어 포함 파일의 내용을 반영합니다.\n"

#: help2man.h2m.PL:174
msgid ""
"Placement of the text within the section may be explicitly requested by "
"using\n"
"the syntax\n"
".RI [< section ],\n"
".RI [= section ]\n"
"or\n"
".RI [> section ]\n"
"to place the additional text before, in place of, or after the default\n"
"output respectively.\n"
msgstr ""
"섹션에 들어간 내용의 위치는 문법을 활용하여 분명하게 요청할 수 있습니다.\n"
".RI [< 섹션 ],\n"
".RI [= 섹션 ]\n"
"또는\n"
".RI [> 섹션 ]\n"
"으로 추가 내용 이전 또는 기본 동작처럼 그 다음에 각각 넣을 수 있습니다.\n"

#: help2man.h2m.PL:185
msgid "AVAILABILITY"
msgstr "가용성"

#: help2man.h2m.PL:186
msgid "The latest version of this distribution is available on-line from:"
msgstr "이 배포본의 최신 버전은 다음 온라인 위치에서 살펴볼 수 있습니다:"
